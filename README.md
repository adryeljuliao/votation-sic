# votation-sic



### Informativos sobre o projeto

Este projeto foi gerado automaticamente pelo Spring Initializr. Tem como principal objetivo realizar votações nas pautas em questão.

- A documentação dessa app está no swagger. 
- A aplicação foi hospedada em nuvem na plataforma Railway.
- O banco de dados também está hospedado em nuvem na plataforma Railway

### Swagger

Para a visualização dos endpoints basta acessar a URL do swagger: https://votation-sic-production.up.railway.app/swagger-ui/index.html







